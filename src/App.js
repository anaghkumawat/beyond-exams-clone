import NavBar from "./Components/NavBar";
import Browse from "./Components/Browse"

function App() {
  return (
    <div className="App">
      <NavBar />
      <Browse />
    </div>
  );
}

export default App;
