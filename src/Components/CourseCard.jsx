import React from 'react'
import styles from './CourseCard.module.css'
import MoreVertIcon from '@mui/icons-material/MoreVert';


const CourseCard = (props) => {
    return (
        <div className={`${styles.CourseCardMain}`}>
            <div style={{backgroundImage: `url(${props.src})`}} className={`${styles.CourseCardImage}`}/>
            <span className={`${styles.CourseCardTitleArea}`}>
                <span className={`${styles.CourseCardTitle}`}>{props.title}</span> <MoreVertIcon />
            </span>
            <span className={`${styles.CourseCardDescription}`}>
                <span>{props.count} Courses</span>
                <span>{props.views} views</span>
            </span>
        </div>
    )
}

export default CourseCard