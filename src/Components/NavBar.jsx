import React, {useState} from 'react'
import SearchIcon from '@mui/icons-material/Search';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import styles from './NavBar.module.css';
import ProfileIcon from "./ProfileIcon";
import MenuIcon from '@mui/icons-material/Menu';

const NavBar = () => {
    const [isSearchActive, setIsSearchActive] = useState(false);

    const handleSearchClick = () => {
        setIsSearchActive(!isSearchActive);
    }

    return (
        <div className={styles.navBarMain}>
            <MenuIcon className={`${styles.navBarMenu} ${isSearchActive ? styles.searchActive : ''}`}/>
            <div className={`${styles.navBarLogo} ${isSearchActive ? styles.searchActive : ''}`} />
            <div className={`${styles.navBarSearchArea} ${isSearchActive ? styles.searchActive : ''}`}>
                <input type="text" className={`${styles.navBarSearchInput} ${isSearchActive ? styles.searchActive : ''}`} placeholder="What do you want to learn today?"/>
                <button className={`${styles.navBarSearchIcon} ${isSearchActive ? styles.searchActive : ''}`} onClick={handleSearchClick}>
                    <SearchIcon />
                </button>
                <button className={`${styles.navBarCoursesButton} ${isSearchActive ? styles.searchActive : ''}`}>
                    Courses
                </button>
                <span className={`${styles.navBarServicesButton} ${isSearchActive ? styles.searchActive : ''}`}>
                    Our services <KeyboardArrowDownIcon />
                </span>
                <ProfileIcon />
            </div>
        </div>
    )
}

export default NavBar